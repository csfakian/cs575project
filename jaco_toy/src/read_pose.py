#! /usr/bin/env python

import roslib
import sys
import copy
import rospy
import actionlib
import kinova_msgs.msg
import tf
#import moveit_commander
#import moveit_msgs.msg
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import String
import std_msgs.msg
from jaco_grasp.srv import ReadPose
import tf2_ros

prefix = 'j2n6s300_'
target = PoseStamped()
fposes = open("valid_poses.txt", 'a+')
count = 0

def callback(data):
    global target
    target = data
    
def servCallback(request):
    global count
    count += 1    
    fposes.write("(%s %s %s)\n"%(target.pose.position.x,target.pose.position.y,target.pose.position.z)) 
    print ("%s: [%f, %f, %f, %s, %s, %s, %s]" % 
           (count,target.pose.position.x,target.pose.position.y,target.pose.position.z,
            target.pose.orientation.x,target.pose.orientation.y,target.pose.orientation.z,target.pose.orientation.w) 
           )
  
#    %(target.pose.position.x,target.pose.position.y,target.pose.position.z,target.pose.orientation.x,target.pose.orientation.y,target.pose.orientation.z,target.pose.orientation.w))
  
#    fposes.close()
    return "Success"
    
def listener():
    rospy.init_node('listener', anonymous=True)
    action_address = '/j2n6s300_driver/out/tool_pose'
    fposes.write("[Writing Robot Poses]\n")
    rospy.Subscriber(action_address, PoseStamped, callback)
    rospy.Service('read_pose', ReadPose, servCallback)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
