# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 15:37:55 2020

@author: csfakian
"""

import cv2
#import numpy as np
#from matplotlib import pyplot as plt
#import argparse
import imutils
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

rospy.init_node('my_jaco_node')
# Print "Hello ROS!" to the Terminal and to a ROS Log file located in ~/.ros/log/loghash/*.log
rospy.loginfo("Hello JACO ROS!")

# Initialize the CvBridge class
bridge = CvBridge()

cv_image = None

# Define a function to show the image in an OpenCV Window
def show_image(img):
    cv2.imshow("Image Window", img)
    cv2.waitKey(3)

# Define a callback for the Image message
def image_callback(img_msg):
    # log some info about the image topic
    global cv_image
    rospy.loginfo(img_msg.header)

    # Try to convert the ROS Image message to a CV2 Image
    try:
        cv_image = bridge.imgmsg_to_cv2(img_msg, "bgr8")
    except CvBridgeError, e:
        rospy.logerr("CvBridge Error: {0}".format(e))
    
def shape_recognition(input_img):
    
#    match_threshold_value = 0.05
    # define hole area image
    hole_area_start_y = 250
    hole_area_end_y = 400
    hole_area_start_x = 130
    hole_area_end_x = 300
    # define shapes area image
    shapes_area_start_y = 250
    shapes_area_end_y = 420
    shapes_area_start_x = 310
    shapes_area_end_x = 470
    # read current rgb image
    img_rgb = input_img
    #img_rgb = cv2.imread('black_fg3.png')
    #img_rgb= cv2.imread('imagergb4.png')
    cv2.imshow("Image", img_rgb)
    cv2.waitKey(0)#DEBUG_MODE
    # change to grayscale image
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    # reduse some noise
    blurred_gray = cv2.GaussianBlur(img_gray, (5, 5), 0)
    # show the the grayscale image
    #cv2.imshow('image gray',blurred_gray) #DEBUG_MODE
    #cv2.waitKey(0) #wait for key press. only for DEBUG_MODE
    # make the the hole area image according to area definitions
    hole_area_image = blurred_gray[hole_area_start_y:hole_area_end_y,hole_area_start_x:hole_area_end_x]
    hole_thresh_value = 45 # define threshold value for hole 
    # Otsu for hole area --> extracts the hole
    ret, hole_thresh_image = cv2.threshold(hole_area_image, hole_thresh_value, 255,0)
    #cv2.imshow('hole shape',hole_thresh_image) # whow the hole image for check #DEBUG_MODE
    #cv2.waitKey(0) #wait for key press. only for DEBUG_MODE
    # check to see if we are using OpenCV 2.X or OpenCV 4
    if imutils.is_cv2() or imutils.is_cv4():
    	(contours, _) = cv2.findContours(hole_thresh_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # check to see if we are using OpenCV 3
    elif imutils.is_cv3():
    	(_, contours, _) = cv2.findContours(hole_thresh_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
      
    #im2, contours, hierarchy = cv2.findContours(hole_thresh_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # Sort the contours 
    hole_contours = sorted(contours, key = cv2.contourArea, reverse = True)
    try:
        # Draw the contour 
        cnt = hole_contours[1] #contours[0] is the whole hole_area image
    except:
        print("hole contours error")
        return None
        
    M = cv2.moments(cnt)
    cX_hole = int(M["m10"] / M["m00"]) + hole_area_start_x
    cY_hole = int(M["m01"] / M["m00"]) + hole_area_start_y
    print("Hole position x: ",cX_hole, "Hole position y: ", cY_hole) #DEBUG_MODE
    
    cnt_hole = cnt
    i = 0
    for term in cnt:
    #    print("term0: ", term[0][0], "term1: ", term[0][1]) #DEBUG_MODE
        cnt_hole[i][0][0] = term[0][0] + hole_area_start_x
        cnt_hole[i][0][1] = term[0][1] + hole_area_start_y
        i = i + 1
        
    #print ("cnt_hole", cnt_hole) #DEBUG_MODE
    #cv2.imshow('image',img_rgb)
    #cv2.waitKey(0)#DEBUG_MODE
    cv2.drawContours(img_rgb, [cnt_hole], contourIdx = -1, color = (0, 255, 0), thickness = 4)
    #cv2.imshow('hole_contours',img_rgb)
    
    cv2.circle(img_rgb, (cX_hole, cY_hole), 7, (255, 255, 255), -1)
    cv2.putText(img_rgb, "hole center", (cX_hole - 20, cY_hole - 20), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 0), 2)
    
    shapes_area_image = blurred_gray[shapes_area_start_y:shapes_area_end_y,shapes_area_start_x:shapes_area_end_x]
    shapes_thresh_value = 55#50
    ret, shapes_thresh_image = cv2.threshold(shapes_area_image, shapes_thresh_value, 255,cv2.THRESH_BINARY_INV)
    #cv2.imshow('shapes shape',shapes_thresh_image) #DEBUG_MODE
    
    if imutils.is_cv2() or imutils.is_cv4():
    	(contours, _) = cv2.findContours(shapes_thresh_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # check to see if we are using OpenCV 3
    elif imutils.is_cv3():
    	(_, contours, _) = cv2.findContours(shapes_thresh_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #im2, contours, hierarchy = cv2.findContours(shapes_thresh_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    ## find contours in the thresholded image
    #contours = cv2.findContours(shapes_thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    #shapes_contours = imutils.grab_contours(contours)
#    cv2.waitKey(1000)#DEBUG_MODE
    ## loop over the contours
    shapes_center = []
    shapes_contours = []
    test = 1 #DEBUG_MODE
    for c in contours[1:]:
        cv2.imshow("Image", img_rgb)
    	# compute the center of the contour 
        M = cv2.moments(c)
        if (M["m00"] != 0) and (cv2.isContourConvex(c) == False):
            cX = int(M["m10"] / M["m00"]) + shapes_area_start_x
            cY = int(M["m01"] / M["m00"]) + shapes_area_start_y
    #        print("cX: ",cX, "cY: ", cY) #DEBUG_MODE
    
            # draw the contour and center of the shape on the image    
            cnt_offset = c
            i=0
            for term in c:
    #            print("term0: ", term[0][0], "term1: ", term[0][1]) #DEBUG_MODE
                cnt_offset[i][0][0] = term[0][0] + shapes_area_start_x
                cnt_offset[i][0][1] = term[0][1] + shapes_area_start_y
                i = i + 1
        
            cv2.drawContours(img_rgb, [cnt_offset], -1, (0, 255, 0), 2)
            cv2.circle(img_rgb, (cX, cY), 2, (255, 255, 255), -1)
            cv2.putText(img_rgb, str(test), (cX, cY - 12), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255), 1)
#            cv2.waitKey(600)#DEBUG_MODE
            # show the image
            cv2.imshow("Image", img_rgb)
            shapes_center.append([cX ,cY])
            shapes_contours.append(c)
            test = test + 1
    
    index = 0
    min_ret = 9999
    for shape in shapes_contours:
        ret = cv2.matchShapes(cnt_hole,shape,1,0.0)
        print(index) #DEBUG_MODE
    #    print(cv2.isContourConvex(shape))
        print(ret) #DEBUG_MODE
        if ret <= min_ret:
            match_shape_index = index
            min_ret = ret
        index = index + 1
    #    cv2.waitKey(0)#DEBUG_MODE
        
    print("match value index", match_shape_index) #DEBUG_MODE
    cv2.drawContours(img_rgb, [shapes_contours[match_shape_index]], -1, (255, 0, 0), 2)
    cv2.putText(img_rgb, "match!!!", (shapes_center[match_shape_index][0] -20, shapes_center[match_shape_index][1] + 25), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255), 1)        
    cv2.imshow("Image", img_rgb)
    print("Match Shape position: ", shapes_center[match_shape_index])

# Initalize a subscriber to the "/camera/rgb/image_raw" topic with the function "image_callback" as a callback
sub_image = rospy.Subscriber("/camera/rgb/image_raw", Image, image_callback)


# Loop to keep the program from shutting down unless ROS is shut down, or CTRL+C is pressed
rate = rospy.Rate(10) # 10hz

while not rospy.is_shutdown():
    if cv_image is not None:
        sub_image.unregister()
        cv2.imwrite("testrgb.jpg", cv_image) 
        rate.sleep()
        #show_image(cv_image)
        shape_recognition(cv_image)
        cv2.destroyAllWindows()

#cv2.waitKey(0)
#cv2.destroyAllWindows()